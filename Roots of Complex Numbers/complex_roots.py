import matplotlib.pyplot as plt
import sys
from numpy import pi, cos, sin, arctan, sqrt, exp, imag

z = complex(-16, -16)
root = 4

PLOT_LINES_BETWEEN_POINTS = True
PLOT_LINES_TO_ORIGIN = True
PLOT_GRID = True
PLOT_POINTS = True
PLOT_CIRCLE = True
POINT_COLOR = 'k'
ORIGIN_COLOR = 'k'
CIRCLE_COLOR = 'b'
SHOW_TITLE = True



def Ck(n, r, theta, k):
    ang = theta/n + 2*k*pi/n
    return r**(1/n) * complex(cos(ang), sin(ang))

def Croots(n, r, theta):
    roots = []
    k = 0
    while k < n:
        ang = theta/n + 2*k*pi/n
        R = r**(1/n) * complex(cos(ang), sin(ang))
        roots.append(R)
        k += 1
    return roots

def plotImag(z, c):
    plt.plot(z.real, z.imag, c)

x0 = z.real
y0 = z.imag
r0 = sqrt(x0**2 + y0**2)
fig, ax = plt.subplots()
plt.plot(0, 0, '.', color=ORIGIN_COLOR)

if x0 == 0:
    if y0 < 0:
        theta0 = -pi/2
    else:
        theta0 = pi/2
else:
    theta0 = arctan(y0/x0)

c = Croots(root, r0, theta0)
for i in range(1,len(c)):
    if PLOT_LINES_TO_ORIGIN:
        plt.plot([0, c[i-1].real], [0, c[i-1].imag], 'ko-')
    if PLOT_LINES_BETWEEN_POINTS:
        plt.plot([c[i-1].real, c[i].real], [c[i-1].imag, c[i].imag], 'ko-')
    if PLOT_POINTS:
        plotImag(c[i-1], '.b')
        plt.annotate("C" + str(i-1), (c[i-1].real + 0.1, c[i-1].imag - 0.1))

if PLOT_LINES_TO_ORIGIN:
    plt.plot([0, c[i].real], [0, c[i].imag], 'ko-')
    
if PLOT_LINES_BETWEEN_POINTS:
    plt.plot([c[i].real, c[0].real], [c[i].imag, c[0].imag], 'ko-')

if PLOT_POINTS:
    plotImag(c[i], '.b')
    plt.annotate("C" + str(i), (c[i].real + 0.1, c[i].imag - 0.1))

if PLOT_CIRCLE:
    Circle1 = plt.Circle((0, 0), r0**(1/root), color=CIRCLE_COLOR, fill=False)
    ax.add_patch(Circle1)

if SHOW_TITLE:
    title = "{} root of the complex number {} + {}i".format(root, z.real, z.imag)
    plt.title(title)

if PLOT_GRID:
    plt.grid()
plt.show()
